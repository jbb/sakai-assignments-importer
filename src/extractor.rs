// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use scraper::ElementRef;
use scraper::{Html, Node, Selector};

use reqwest::blocking::Client;
use reqwest::header::{
    HeaderMap, HeaderValue, COOKIE, REFERER, UPGRADE_INSECURE_REQUESTS, USER_AGENT,
};

use chrono::{DateTime, Local, NaiveDateTime, TimeZone};

use crate::types::{Assignment, Attachment, AttachmentType, Module};

const FF_USER_AGENT: &str = "Mozilla/5.0 (X11; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0";

pub fn build_generic_geaders(referer: &str, session_cookie: &str) -> HeaderMap {
    let mut headers = HeaderMap::new();
    headers.insert(USER_AGENT, HeaderValue::from_static(FF_USER_AGENT));
    headers.insert(REFERER, HeaderValue::from_str(referer).unwrap());
    headers.insert(COOKIE, HeaderValue::from_str(session_cookie).unwrap());
    headers.insert(UPGRADE_INSECURE_REQUESTS, HeaderValue::from_static("1"));

    headers
}

fn fetch_task_tool_url(module_url: &str, session_cookie: &str) -> Option<String> {
    let res = Client::new()
        .get(module_url)
        .headers(build_generic_geaders(
            "https://mycampus.imp.fu-berlin.de/portal",
            session_cookie,
        ))
        .send()
        .unwrap();

    let html = res.text().unwrap();

    let document = Html::parse_document(&html);
    let assignment_icon_selector = Selector::parse(
        r#"span[class="Mrphs-toolsNav__menuitem--icon icon-sakai--sakai-assignment-grades "]"#,
    )
    .unwrap();
    let active_assignment_icon_selector = Selector::parse(r#"span[class="Mrphs-toolsNav__menuitem--icon icon-sakai--sakai-assignment-grades  icon-active"]"#).unwrap();

    let assignments_icon_opt = document
        .select(&assignment_icon_selector)
        .next()
        .or_else(|| document.select(&active_assignment_icon_selector).next());
    if let Some(assignments_icon) = assignments_icon_opt {
        let button = assignments_icon.parent().unwrap().value();
        if let Node::Element(element) = button {
            let href = element.attr("href").unwrap();
            return Some(href.to_string());
        }
    }

    None
}

fn extract_attachment_urls(url: &str, session_cookie: &str) -> Vec<Attachment> {
    let mut attachments: Vec<Attachment> = Vec::new();

    // Fetch details page
    let res = Client::new()
        .get(url)
        .headers(build_generic_geaders(url, session_cookie))
        .send()
        .unwrap();

    let details_html = res.text().unwrap();
    let document = Html::parse_document(&details_html);
    let attachment_list_selector = Selector::parse(r#"ul[class="attachList indnt1"]"#).unwrap();

    let mut index = -1;
    let link_selector = Selector::parse("a").unwrap();

    for attachment_list in document.select(&attachment_list_selector) {
        index += 1;
        attachments.append(
            &mut attachment_list
                .select(&link_selector)
                .map(|link| Attachment {
                    url: link.value().attr("href").unwrap().to_string(),
                    name: link.inner_html(),
                    attachment_type: match index {
                        0 => AttachmentType::Resource,
                        1 => AttachmentType::Submitted,
                        2 => AttachmentType::Returned,
                        _ => AttachmentType::Unknown,
                    },
                })
                .collect(),
        );
    }

    attachments
}

fn extract_row(
    module_name: &str,
    session_cookie: &str,
    row: ElementRef,
    fetch_attachment_urls: bool,
) -> Option<Assignment> {
    let td_selector = Selector::parse("td").unwrap();
    let tds = &mut row.select(&td_selector);
    if tds.count() > 4 {
        tds.next();
        let title = tds.next().unwrap();
        let status = tds.next().unwrap();
        let open_date = tds.next().unwrap();
        let due_date = tds.next().unwrap();

        let text_selector = Selector::parse("a").unwrap();
        let due_date_selector = Selector::parse("span").unwrap();

        let title_elem = title.select(&text_selector).next().unwrap();

        let title = title_elem.inner_html();
        let url = title_elem.value().attr("href").unwrap().to_owned();
        let status = status.inner_html().replace("\t", "").replace("\n", "");
        let open_date =
            parse_sakai_time(&open_date.inner_html().replace("\t", "").replace("\n", ""));
        let due_date = parse_sakai_time(
            &due_date
                .select(&due_date_selector)
                .next()
                .unwrap()
                .inner_html(),
        );

        let attachments: Vec<Attachment> = match fetch_attachment_urls {
            true => extract_attachment_urls(&url, session_cookie),
            false => Vec::new(),
        };

        Some(Assignment {
            attachments,
            title,
            status,
            open_date,
            due_date,
            module_name: module_name.to_string(),
            url,
        })
    } else {
        None
    }
}

fn fetch_tasks(
    module_url: &str,
    module_name: &str,
    session_cookie: &str,
    fetch_attachment_urls: bool,
) -> Vec<Assignment> {
    let tasks_url_result = fetch_task_tool_url(module_url, session_cookie);
    //println!("{:?}", tasks_url_result);
    if let Some(url) = tasks_url_result {
        let res = Client::new()
            .get(&url)
            .headers(build_generic_geaders(&url, session_cookie))
            .send()
            .unwrap();

        let html = res.text().unwrap();

        let document = Html::parse_document(&html);
        let assignments_form_selector =
            Selector::parse(r#"form[name="listAssignmentsForm"]"#).unwrap();
        if let Some(element) = document.select(&assignments_form_selector).next() {
            let table_selector = Selector::parse("table").unwrap();
            let table = element.select(&table_selector).next().unwrap();

            let row_selector = Selector::parse("tr").unwrap();

            return table
                .select(&row_selector)
                .map(|row| extract_row(module_name, session_cookie, row, fetch_attachment_urls))
                .filter(|assignment| assignment.is_some())
                .map(|assignment| assignment.unwrap())
                .collect();
        }
        println!("Module {} has an empty tasks table", module_name);
        return Vec::new();
    }

    println!("Module {} does not have tasks enabled", module_name);

    Vec::new()
}

fn fetch_modules(session_cookie: &str) -> Vec<Module> {
    let res = Client::new()
        .get("https://mycampus.imp.fu-berlin.de/portal")
        .headers(build_generic_geaders(
            "https://mycampus.imp.fu-berlin.de/",
            session_cookie,
        ))
        .send()
        .unwrap();

    let html = res.text().unwrap();
    let document = Html::parse_document(&html);

    let sites_selector = Selector::parse(r#"div[class="fav-sites-term"]"#).unwrap();
    let sites = document.select(&sites_selector).next();
    if let Some(sites) = sites {
        let list_item_selector = Selector::parse(r#"div[class="fav-title "]"#).unwrap(); // Whitespace in selector is important

        let name_selector = Selector::parse(r#"span[class="fullTitle"]"#).unwrap();
        let url_selector = Selector::parse("a").unwrap();

        return sites
            .select(&list_item_selector)
            .map(|item| Module {
                name: item.select(&name_selector).next().unwrap().inner_html(),
                url: item
                    .select(&url_selector)
                    .next()
                    .unwrap()
                    .value()
                    .attr("href")
                    .unwrap()
                    .to_string(),
            })
            .collect();
    } else {
        println!("Can not find sites button, probably the login failed. Please check / update your session cookie.");
        std::process::exit(1);
    }
}

/// Parses the datetime as the system time zone
/// Currently supports German and English
///
/// Example en_US: Timestr Nov 20, 2020 11:00 am
/// Example de_DE: 30.11.2020 09:00
fn parse_sakai_time(timestr: &str) -> DateTime<Local> {
    //println!("Timestr: {:?}", timestr);
    // German
    let naive_date = NaiveDateTime::parse_from_str(timestr, "%d.%m.%Y %k:%M")
        // US English
        // TODO Why does this not work when pm occurs?
        .or_else(|_| NaiveDateTime::parse_from_str(timestr, "%b %e, %Y %H:%M %P"));
    //println!("Parsed: {:?}", naive_date);
    Local.from_local_datetime(&naive_date.unwrap()).unwrap()
}

pub fn fetch_all_tasks(session_cookie: &str, fetch_attachment_urls: bool) -> Vec<Assignment> {
    fetch_modules(session_cookie)
        .iter()
        .flat_map(|module| {
            fetch_tasks(
                &module.url,
                &module.name,
                session_cookie,
                fetch_attachment_urls,
            )
        })
        .collect()
}

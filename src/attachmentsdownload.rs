use crate::extractor::build_generic_geaders;
use crate::types::{Assignment, AttachmentType};

use reqwest::blocking::Client;

use std::fs::File;

fn attachment_type_to_dir_name(attachment_type: AttachmentType) -> &'static str {
    match attachment_type {
        AttachmentType::Resource => "resources",
        AttachmentType::Returned => "returned",
        AttachmentType::Submitted => "submitted",
        AttachmentType::Unknown => "other",
    }
}

fn download_attachments(assignment: Assignment, session_cookie: &str, attachments_dir: &str) {
    for attachment in assignment.attachments {
        let mut res = Client::new()
            .get(&attachment.url)
            .headers(build_generic_geaders(
                "https://mycampus.imp.fu-berlin.de/portal",
                session_cookie,
            ))
            .send()
            .unwrap();

        let target_dir = format!(
            "{}/{}/{}/{}",
            attachments_dir,
            assignment.module_name,
            assignment.title,
            attachment_type_to_dir_name(attachment.attachment_type)
        );
        std::fs::create_dir_all(&target_dir).unwrap();
        let mut outfile = File::create(format!("{}/{}", target_dir, attachment.name)).unwrap();
        res.copy_to(&mut outfile).unwrap();
    }
}

pub fn download_attachments_of_assignments(
    assignments: Vec<Assignment>,
    session_cookie: &str,
    attachments_dir: &str,
) {
    for assignment in assignments {
        download_attachments(assignment, session_cookie, attachments_dir);
    }
}

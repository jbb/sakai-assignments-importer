// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use chrono::{DateTime, Local};

use std::fmt::Display;

#[derive(Debug, Clone)]
pub enum AttachmentType {
    Resource,
    Submitted,
    Returned,
    Unknown,
}

#[derive(Debug, Clone)]
pub struct Attachment {
    pub url: String,
    pub name: String,
    pub attachment_type: AttachmentType,
}

#[derive(Debug, Clone)]
pub struct Assignment {
    pub title: String,
    pub status: String,
    pub open_date: DateTime<Local>,
    pub due_date: DateTime<Local>,
    pub module_name: String,
    pub url: String,
    pub attachments: Vec<Attachment>,
}

impl Display for Assignment {
    fn fmt(&self, formatter: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        formatter.write_str(&format!("\n# {}\n", self.title))?;
        formatter.write_str(&format!("Url: {}\n", self.url))?;
        formatter.write_str(&format!("Status: {}\n", self.status))?;
        formatter.write_str(&format!(
            "Open Date: {}\n",
            self.open_date.naive_local().format("%d.%m.%Y %k:%M")
        ))?;
        formatter.write_str(&format!(
            "Due Date: {}\n",
            self.due_date.naive_local().format("%d.%m.%Y %k:%M")
        ))?;
        formatter.write_str(&format!("Module: {}", self.module_name))?;
        formatter.write_str(&format!("Attachments: {:?}", self.attachments))?;
        Ok(())
    }
}

#[derive(Debug)]
pub struct Module {
    pub url: String,
    pub name: String,
}

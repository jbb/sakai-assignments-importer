// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

mod attachmentsdownload;
mod extractor;
mod ical;
mod types;
use crate::types::Assignment;

use std::fs::read_to_string;

use chrono::Local;

use clap::{App, Arg};

fn print_open_assignments(mut all_assignments: Vec<Assignment>) {
    // Sort by due date
    all_assignments.sort_by(|task1, task2| task1.due_date.cmp(&task2.due_date));

    // Filter and print still to-do tasks
    all_assignments
        .into_iter()
        .filter(|task| task.due_date > Local::now())
        .for_each(|assignment| println!("{}", assignment));
}

fn read_session_file() -> String {
    let mut config_path = dirs::config_dir().unwrap();
    config_path.push("fu-whiteboard/whiteboard_session_cookie");
    if let Ok(string) = read_to_string(config_path) {
        string.replace("\n", "")
    } else {
        println!(
            "session_cookie missing. Please add a session cookie into the session_cookie file."
        );
        std::process::exit(1);
    }
}

fn main() {
    let matches = App::new("sakai-assignments-importer")
        .version("1.0")
        .author("Jonah Brüchert <jbb@kaidan.im>")
        .about("Import your assignments into your calendar")
        .arg(
            Arg::with_name("vdir")
                .value_name("PATH")
                .long("vdir")
                .help("Saves the assignments as multiple ics files in the given directory")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("ics")
                .value_name("FILE")
                .long("ics")
                .help("Saves the assignments as single ics file to the given path")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("attachments-dir")
                .value_name("TASKS_DIR")
                .long("attachments-dir")
                .help("Saves the assignment attachments to the given directory")
                .takes_value(true),
        )
        .get_matches();

    let session_cookie = read_session_file();
    let assignments = extractor::fetch_all_tasks(
        &session_cookie,
        matches.value_of("attachments-dir").is_some(),
    );
    print_open_assignments(assignments.clone());

    if let Some(vdir) = matches.value_of("vdir") {
        ical::ical_export_vdir(vdir, assignments)
    } else if let Some(ics_file) = matches.value_of("ics") {
        ical::ical_export_single(ics_file, assignments)
    } else if let Some(attachments_dir) = matches.value_of("attachments-dir") {
        attachmentsdownload::download_attachments_of_assignments(
            assignments,
            &session_cookie,
            attachments_dir,
        );
    }
}

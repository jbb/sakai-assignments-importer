// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use ics::properties::{
    Categories, Completed, Description, DtEnd, DtStart, Due, LastModified, Status, Summary, URL,
};
use ics::{ICalendar, ToDo};

use chrono::{DateTime, Local};

use hmac_sha256::Hash;

use crate::types::Assignment;

fn ical_date(datetime: DateTime<Local>) -> String {
    format!("{}", datetime.naive_local().format("%Y%m%dT%H%M%SZ"))
}

fn hash(assignment: &Assignment) -> String {
    // Mix all information that makes an assignment unique
    let hash_input = assignment.title.clone()
        + &assignment.module_name
        + &assignment.due_date.to_rfc2822()
        + &assignment.open_date.to_rfc2822();
    let uid = Hash::hash(hash_input.as_bytes());
    base64::encode_config(uid, base64::URL_SAFE)
}

fn assignment_to_ical(assignment: &Assignment) -> ToDo<'static> {
    let uid = hash(assignment);

    let mut event = ToDo::new(uid, ical_date(Local::now()));
    event.push(LastModified::new(ical_date(Local::now())));
    event.push(DtStart::new(ical_date(assignment.open_date)));
    event.push(DtEnd::new(ical_date(assignment.due_date)));
    event.push(Due::new(ical_date(assignment.due_date)));

    if assignment.due_date < Local::now() {
        event.push(Completed::new(ical_date(assignment.due_date)));
    }

    fn needs_action(string: &str) -> bool {
        string.contains("Nicht begonnen") || string.contains("Not Started")
    }

    fn in_progress(string: &str) -> bool {
        string.contains("In Bearbeitung") || string.contains("In progress")
    }

    fn completed(string: &str) -> bool {
        string.contains("Eingereicht") || string.contains("Submitted")
    }

    fn is_returned(string: &str) -> bool {
        string.contains("Zurückgesendet") || string.contains("Returned")
    }

    event.push(match assignment.status.as_str() {
        _ if needs_action(&assignment.status) => Status::needs_action(),
        _ if in_progress(&assignment.status) => Status::in_process(),
        _ if completed(&assignment.status) || is_returned(&assignment.status) => {
            Status::completed()
        }
        _ => Status::confirmed(),
    });
    event.push(Categories::new("EDUCATION"));
    event.push(Summary::new(
        assignment.module_name.clone() + " " + &assignment.title,
    ));
    event.push(URL::new(assignment.url.clone()));
    // Fallback because most applications don't display urls for tasks
    event.push(Description::new(assignment.url.clone()));

    event
}

pub fn ical_export_single(path: &str, assignments: Vec<Assignment>) {
    let mut calendar = ICalendar::new("2.0", "-//sakai-api//SakaiAPI 1.0//EN");

    // add event to calendar
    for assignment in assignments {
        calendar.add_todo(assignment_to_ical(&assignment));
    }

    calendar.save_file(path).unwrap();
}

pub fn ical_export_vdir(path: &str, assignments: Vec<Assignment>) {
    let assignments_path = &format!("{}/assignments", path);
    std::fs::create_dir_all(assignments_path).expect(&format!("Faild to create vdir path {}", assignments_path));
    // add event to calendar
    for assignment in assignments {
        let mut calendar = ICalendar::new("2.0", "-//sakai-api//SakaiAPI 1.0//EN");
        calendar.add_todo(assignment_to_ical(&assignment));
        calendar
            .save_file(&format!("{}/{}.ics", assignments_path, hash(&assignment)))
            .unwrap();
    }
}

# FU Whiteboard client

Currently only supports listing the assignments you still have to do, and exporting them into an ics calendar.

## How to use

Use a webbrowser to go to the whiteboard.
Open the developer tools and start a network analysis.
Then log into the whiteboard.
Using the developer tools, look at the Cookie header of a request to the whiteboard.
Copy the contents, and put it into `~/.config/fu-whiteboard/whiteboard_session_cookie`.
Please note that the cookie will expire if you don't use it at least every few hours.
Running the assignments import using systemd timers or cron is recommended.

Then run `cargo install --git https://gitlab.spline.inf.fu-berlin.de/jbb/sakai-assignments-importer` to install the project.
Afterwards you can run `sakai-assignments-importer`.

## Synchronize with a Nextcloud instance

Install vdirsyncer. Afterwards copy the following config file to `~/.config/vdirsyncer/config`, and edit the url, username, password and path.
```toml
[general]
status_path = "~/.vdirsyncer/status/"

[pair assignments_calendar]
a = "assignment_calendar_local"
b = "assignment_calendar_remote"
collections = ["from a"]
conflict_resolution = "a wins"

[storage assignment_calendar_local]
type = "filesystem"
path = "~/.calendarsync"
fileext = ".ics"

[storage assignment_calendar_remote]
type = "caldav"

url = "https://jbb.d.pboehm.de/remote.php/dav/calendars"
username = "<username>"
password = "<password>"
```

To make sure the syncdirectory exists, create it: `mkdir ~/.calendarsync/`.

Afterwards import the assignments into the synchronization folder using
`sakai-assignments-importer --vdir ~/.calendarsync/`.

Now run`vdirsyncer discover`.
vdirsyncer should see the new "assignments" calendar.

To put it on the server, run
`vdirsyncer sync`.
